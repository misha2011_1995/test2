﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var listPosts = db.Users.ToList();
            //string userId = User.Identity.GetUserId();
            //var userData = db.Users.Find(userId);
            return View("Index", listPosts);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            Response.Redirect("About", false);
            ViewBag.Message = "Your contact page.";

            return View(); 
        }
        public ActionResult PartialView()
        {          

            return View(); 
        }
    }
}